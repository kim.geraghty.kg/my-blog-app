from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView

# from django.shortcuts import get_object_or_404

from .models import Post
from .forms import PostForm


# Create your views here.
def show_post_detail(request, pk):
    post = Post.objects.get(pk=pk)
    context = {"post": post}
    return render(request, "posts/detail.html", context)


def list_all_posts(request):
    # 1.  get the data
    #  - use the django model methods to get the data
    posts = Post.objects.all()

    # 2. pass the data to the template
    # make a context dictionary with "posts" as the key
    # and all the posts from the database as the value
    context = {"posts": posts}

    # 3. return the template to the browser
    # return an http response by returning the django render function,
    # which calls the template and gives it the data in the context
    return render(request, "posts/list.html", context)


def create_post(request):
    form = PostForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("post_list")
    context = {"form": form}
    return render(request, "posts/create.html", context)


# 1. create a variable named form
#   validate form, save, etc
# 2. take them back to lists all posts
# 3. create our context with key "form" and value of the form we made

# replacing our function based view with a class based view:


class CreatePostView(CreateView):
    model = Post
    template_name = "posts/create.html"
    fields = ["title", "content"]
    success_url = "/"
