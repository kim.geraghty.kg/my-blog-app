from django.urls import path

from posts.views import list_all_posts, show_post_detail, create_post
from posts.views import CreatePostView

urlpatterns = [
    path("<int:pk>/", show_post_detail, name="post_detail"),
    path("", list_all_posts, name="post_list"),
    path("create", CreatePostView.as_view(), name="create_post"),
    # path("create", create_post, name="create_post"),
]


# CreatePostView is not a view function, it's a class
# so we have to add: .as_view when we connect it to our url patterns
# see above
