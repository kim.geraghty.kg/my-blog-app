from django.db import models


# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    content = models.TextField()


class Comment(models.Model):
    author = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    content = models.TextField(max_length=1000)
    post = models.ForeignKey(
        "Post", related_name="comments", on_delete=models.CASCADE
    )


class Keyword(models.Model):
    word = models.CharField(max_length=20)
    posts = models.ManyToManyField("Post", related_name="keywords")


class Rating(models.Model):
    post = models.ForeignKey(
        "Post", related_name="ratings", on_delete=models.CASCADE
    )
    rating = models.PositiveSmallIntegerField()
